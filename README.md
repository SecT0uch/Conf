# Conf

This repo contains my configuration files.

## Fish

Install **fish** package and, for each user :

```
mkdir ~/.config/fish/functions
cp Conf/.config/fish/functions/* ~/.config/fish/functions/
chsh -s `which fish`

```

## Greeting

For greeting messages to work execute `sudo apt-get install fortunes fortunes-debian-hints` or `yay -Sy fortune-mod`
