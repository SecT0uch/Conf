set normal (set_color normal)
set magenta (set_color magenta)
set yellow (set_color yellow)
set green (set_color green)
set red (set_color red)
set gray (set_color -o black)

# Fish git prompt
set __fish_git_prompt_color_branch yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red
set __fish_git_prompt_color_suffix red

# Status Chars
#set __fish_git_prompt_char_dirtystate '⚡'
#set __fish_git_prompt_char_stagedstate '→'
#set __fish_git_prompt_char_untrackedfiles '☡'
#set __fish_git_prompt_char_stashstate '↩'
#set __fish_git_prompt_char_upstream_ahead '+'
#set __fish_git_prompt_char_upstream_behind '-'

function fish_prompt
	and set line red
    or set line red
    tty | string match -q -r tty
    and set tty tty
    or set tty pts

    set_color $line
    echo -n '╭─['

    if test $USER = root -o $USER = toor
        set_color red
    else
        set_color normal
    end
    echo -n $USER

    set_color -o yellow
    echo -n @

    set_color -o cyan
    echo -n (prompt_hostname)

    set_color normal    # Leave it here (bug bold persistant)  
    set_color $line 
    echo -n ']'
    echo -n '─'
    echo -n '['

    set_color white
    echo -n (pwd|sed "s=$HOME=~=")

    set_color $line
    echo -n ]

    echo -n (__fish_git_prompt)

    if type -q acpi
        if [ (acpi -a 2> /dev/null | string match -r off) ]
            echo -n '─['
            set_color -o red
            echo -n (acpi -b|cut -d' ' -f 4-)
            set_color -o $line
            echo -n ']'
        end
    end

    echo

    set_color normal
    for job in (jobs)
        set_color $line
        if [ $tty = tty ]
            echo -n '; '
        else
            echo -n '│ '
        end
        set_color brown
        echo $job
    end

    set_color $line
    echo -n '╰──╼'
    set_color -o yellow

    echo -n ' $ '
    set_color normal
end
